#!/bin/bash.

# Создание файла подкачки.
# Этот сценарий должен запускаться с правами root.

ROOT_UID=0         # Для root -- $UID 0.
E_WRONG_USER=65    # Не root?

FILE=/swap
BLOCKSIZE=1024
MINBLOCKS=40
SUCCESS=0

if [ "$UID" -ne "$ROOT_UID" ]
then
  echo; echo "Этот сценарий должен запускаться с правами root."; echo
  exit $E_WRONG_USER
fi


blocks=${1:-$MINBLOCKS}          #  По-умолчанию -- 40 блоков,
                                 #+ если размер не задан из командной строки.
# Ниже приводится эквивалентный набор команд.
# --------------------------------------------------
# if [ -n "$1" ]
# then
#   blocks=$1
# else
#   blocks=$MINBLOCKS
# fi
# --------------------------------------------------


if [ "$blocks" -lt $MINBLOCKS ]
then
  blocks=$MINBLOCKS              # Должно быть как минимум 40 блоков.
fi


echo "Создание файла подкачки размером $blocks блоков (KB)."
dd if=/dev/zero of=$FILE bs=$BLOCKSIZE count=$blocks  # "Забить" нулями.

mkswap $FILE $blocks             # Назначить как файл подкачки.
swapon $FILE                     # Активировать.

echo "Файл подкачки создан и активирован."

exit $SUCCESS
