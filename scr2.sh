
#!/bin/bash
# default values
sec=0
min=0
hour=0
day=0
week=0
# read line arguments
while [ $# -gt 0 ]; do
    case $1 in
        -s|--sec)
            sec=$2
            shift
            ;;
        -m|--min)
            min=$2
            shift
            ;;
        -h|--hour)
            hour=$2
            shift
            ;;
        -d|--day)
            day=$2
            shift
            ;;
        -w|--week)
            week=$2
            shift
            ;;
    esac
    shift
done
# calc destination time
cur_time=`date -u +%s`
dst_time=$(($cur_time - $sec - $min*60 - $hour*3600 - $day*86400 - $week*604800));
#iter files in log_dir/
for log_file in `find /var/log/ -type f`; do
    #get date of last modification of file in human format
    mod_date=`date -r $log_file '+%Y-%m-%d %H:%M:%S'`
    #get date of last modification of file in unix format
    mod_date_unix=`date -u +%s -r $log_file`
    if [[ $mod_date_unix < $dst_time ]] ; then
        echo "   -" $mod_date $log_file
        rm $log_file
    else
        echo "   +" $mod_date $log_file
    fi
done
